const menuButton = document.getElementById("menu-button");
const menuList = document.querySelector(".menu");

menuButton.addEventListener("click", () => {
  menuList.classList.toggle("-active");
});

const videoCover = document.getElementById("video-cover");
const videoPlayer = document.getElementById("video-player");

videoCover.addEventListener("click", () => {
  const videoUrl = "assets/video/video.mp4";
  videoPlayer.setAttribute("src", videoUrl);

  videoPlayer.play();
});

const items = document.querySelectorAll(".accordion .item");

items.forEach((item) => {
  const title = item.querySelector(".title");

  title.addEventListener("click", () => {
    item.classList.toggle("-active");
  });
});

const modal = document.querySelector(".modal-wiki");
const openModalButton = document.getElementById("button-modal");
const closeModalButton = document.getElementById("close-modal");
const extractDiv = document.querySelector(".extract");

fetch(
  "https://pt.wikipedia.org/w/api.php?action=query&format=json&origin=*&prop=extracts&pageids=736"
)
  .then((response) => response.json())
  .then((data) => {
    const content = data.query.pages["736"].extract;
    extractDiv.innerHTML = content;
  })
  .catch((error) => console.error(error));

openModalButton.addEventListener("click", () => {
  modal.classList.add("modal-wiki-active");
});

closeModalButton.addEventListener("click", () => {
  modal.classList.remove("modal-wiki-active");
});
